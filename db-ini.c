#define COMMAND_SIZE 64
#define FORMAT "python3 dataInsert.py %d %.2f %.2f %.2f"

int iniDB(int mode, float dallasTemperature, float dhtTemperature,float dhtHumidity) {
    char command[COMMAND_SIZE];

    //Place the numbers to format to get command for running python script
    sprintf(command, FORMAT, mode, dallasTemperature, dhtTemperature, dhtHumidity);
    system(command);
    return 0;
}
