import sys
from time import sleep
import urllib.request

#sys.argv[1] = mode
#Modes:
#0 = all
#1 = Dallas 18b20 Only
#2 = DHT22 Only

#Change arguments to usable values 
dallasTemperature = float(sys.argv[2])
dhtTemperature = float(sys.argv[3])
dhtHumidity = float(sys.argv[4])

#Url Variables
baseURL = "http://api.thingspeak.com/update?api_key=9SD3X0788YX2AKWD"
dallasTemperatureField = "&field1="
dhtTemperatureField = "&field2="
dhtHumidityField = "&field3="

#all
if(int(sys.argv[1]) == 0):
    #Post values 
    try:
        f = urllib.request.urlopen(baseURL + dallasTemperatureField + str(dallasTemperature) 
        + dhtTemperatureField + str(dhtTemperature) + dhtHumidityField + str(dhtHumidity))
        f.read()
        f.close()
        print("Write was a success\n\n")
    #Print fail message if failed post
    except:
        print("Write failed\n\n")


#Dallas 18b20 Only
elif(int(sys.argv[1]) == 1):
    #Post values 
    try:
        f = urllib.request.urlopen(baseURL + dallasTemperatureField + str(dallasTemperature))
        f.read()
        f.close()
        print("Write was a success\n\n")
    #Print fail message if failed post
    except:
        print("Write failed\n\n")


#DHT22 Only
elif(int(sys.argv[1]) == 2):
    #Post values 
    try:
        f = urllib.request.urlopen(baseURL + dhtTemperatureField +  str(dhtTemperature)
        + dhtHumidityField + str(dhtHumidity))
        f.read()
        f.close()
        print("Write was a success\n\n")
    #Print fail message if failed post
    except:
        print("Write failed\n\n")
