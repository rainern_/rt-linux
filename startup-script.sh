#!/bin/sh

#Check if there is runnable main file
#Yes = Run it
#No  = Compile and run it 
mainFile=/home/pi/rt-linux/main
if [ -f "$mainFile" ]; then
    /home/pi/rt-linux/main
else 
    gcc ./main.c -o ./main -Wall -pthread -lwiringPi -lwiringPiDev -lrt
    /home/pi/rt-linux/main
fi
