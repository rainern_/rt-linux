# Huomiot
- Kyseinen projekti on kouluprojekti. Eli dokumentaatio on kirjoitettu opettajalle eikä yleisille lukijoille, jotka etsivät järkevää tai järjestelmällistä luettevaa. 
- Dokumentaatio on oppimispäiväkirjan tyylinen 

# Realtime Raspberry Pi 4b Project

## Open Huomiot:
- SD Kortilla oleva RT linuxia on käytetty ssh yhteydellä. Selitykset on ongelma kohdassa #1, mutta tämönen pikanen heads up...
- Päiväkirja on suurimaksi osaksi muistioita miks tein asioita ja screen shotteja rt patch vaiheesta. Itse koodi on kommenteja täynnä ja uskon, että niitä ei tarvitse toistaa uudestaan päiväkirjassa.

## Päiväkirja
### Alku Sanat  ja ajatukset
Real time asiat ovat in general aika tuntemattomia, vaikka niitä tuli käytyä vähän muissa opinoissa. Henkilökohtaisesti RT ei niin kiinnosta, koska tämä ei ole ollut itselle relevantti topic itelle. 
Real time linux ei ole ennen tullut vastaan mutta veikkaus on, että kyse on siitä että muutetaan linuxia niin, että asiat menee ilman interuptia tai jotkin toiminta muodot on vaihdettu nopeampiin versioihin...

#### Muutaman artikkelin ja viedon jälkeen:
Olin joten kuten oikeassa... esim RT_Preempt patch tekee/antaa mahdollisuusen tehdä kernelistä "fully preemptable" ja lisää RT schedulereitä. Sisäset muutokset on: 

- Intteruptit toimii threadeilla
- mutexit korvaa spin lockit
- Priority perinnäisyys laajenee kerneliin asti

Itse Preempt_RT tavoitteet on tehdä 100% preempt kernel (joka on mahdotonta), poistaa keskeytykset käytöstä, poistaa muunlaiset ennakointi toimet ja minimoida viive niin pieneksi kuin voi.

Omasta mielestä paras quote mitä näin/kuulin oli: Jos sinun pitää miettiä tarvitsetko realtimeä, niin luultavasti vastaus on ei. 

Muita hyviä vinkejä mitä näin oli classinen älä käytä 99 priorityä vaikka "voit" (priority on 0-99), käytä mmapia datan siirtoon(shared memory).

"The big advantage of PREEMPT_RT over other real-time Linux implementations is that PREEMPT_RT makes Linux itself real-time, where as the others usually create a small micro kernel that runs like a hypervisor and Linux runs as a task. That’s not really Linux. Your real-time tasks must be modified to communicate with the micro kernel.

With PREEMPT_RT, if your program runs on the stock Linux kernel, it runs on PREEMPT_RT as well. You can easily make a “real-time bash”. Our kernel supports all the posix commands to modify the priority of the tasks in your system. PREEMPT_RT makes interrupts run as threads, so you have the ability to modify the priorities of interrupts as well, and have user space tasks run at even a higher priority than interrupts. Having interrupts run as threads lets you prioritize the interrupt handlers even when the hardware does not support it."
- Steven Rostedt (https://linuxfoundation.org/blog/intro-to-real-time-linux-for-embedded-developers/)


Näistä sai ihan ok kuvan RT:stä linuxilla, mutta saa nähä miten käytänössä tapahtuu...

### Moodle Kysymykset:
1. RT Patch on nimensä mukaan patch/lisäosa linux kerneliin, joka antaa sille real time ominaisuuksia. Real time ominaisuuksilla saadaan suurempi hallittavuus koneen tekemisistä antamalla enemmän prioriteetti asetuksia ja poistamalla hallittomia viivästyksiä. 

2. PREEMPT_RT = “The PREEMPT_RT patch (aka the -rt patch or RT patch) makes Linux into a real-time system”. (fully preemptible (real-time))

3. Scheduler on nimensä mukaan "asia", joka päättää mitä asioita tietokone pyörittää ja millon. Itse Priority on asia/arvo, jonka perusteella Scheduler päättää mitä tehdä. Tämä on "hard" päätös eikä ehdotus kuten nice-arvo. Esim Jos minulla on 2 ohjelmaa pyörimässä joilla on molemilla sama policy ja 1 ohjelmalla on prio 20 ja 2 ohjelmalla on prio 40. Scheduler priorisoi 2 ohjelmaa.

Priorityä pitää käyttää RT dht22 luku prosessissa siten, että RT prosessin priority on isompi kuin norm taskien, mutta ei niin iso että koko systeemi ei enää toimi tai jää hwÖn takia jumiin. Tietenkin käytää RT policyä myös. 

4. Ei ole ny ihan varma mitä tarkalleen haet tällä, että oisko pitänyt laitaa linux kerneliä, shm:iä ja miten se toimii ohjelman kaa, mutta tässä on jonkinlainen piirrus prosesseista ja niiden priosta...

![prio-dia](/img/priority-test/prio-dia.png)


### Original Prep
Nämä vaiheet ei ole kovin merkittäviä oppimisen pohjalta, mutta päätin mainita nämä silti lyhyesti:
1. Asennetaan uusin Raspbian OS kortille, softa löytyy täältä: https://www.raspberrypi.org/software/
2. Laitetaan kortti raspberry pi:hin(Oma on 4B 4GB versio)
3. Mene ohjatun install prosessin läpi...
4. sudo apt update && sudo apt upgrade
5. uname -a
6. sudo apt install git bc bison flex libssl-dev make

### Hardware
Setup:

![Hardware](/img/hardware/hw.png)

Gpio List:

    GPIO 18: DHT22
    GPIO  4: Dallas 18b20
    GPIO 23: Red LED
    GPIO 15: Yellow LED
    GPIO 14: Blue LED

![Hardware](/img/hardware/pinout.png)


### RT Kernel
Ohjeet rt_patychin asentamiseen löytyy annetusta materiaalista ja kernelin asentamiseen löytyy sivuilta https://www.raspberrypi.org/documentation/linux/kernel/building.md 

#### Oma Prosessi: 

    mkdir kernel && cd kernel
    git clone https://github.com/raspberrypi/linux.git -b rpi-4.19.y-rt
    cd linux
    KERNEL=kernel7l
    make bcm2711_defconfig
    make menuconfig

Muutettavat asetukset: Fully preempt RT, High res timer support ja time = 1000Hz

    make -j4 zImage modules dtbs
    sudo make modules_install
    sudo cp arch/arm/boot/dts/*.dtb /boot/
    sudo cp arch/arm/boot/dts/overlays/*.dtb* /boot/overlays/
    sudo cp arch/arm/boot/dts/overlays/README /boot/overlays/
    sudo cp arch/arm/boot/zImage /boot/$KERNEL.img

Step by step kuvat tekemisestä: 

![Kernel-Prep](/img/kernel/setup.png)

![Kernel-Config](/img/kernel/config.png)

![Kernel-Makemenu1](/img/kernel/menu1.png)

![Kernel-Makemenu2](/img/kernel/menu2.png)

![Kernel-Makemenu3](/img/kernel/menu3.png)

![Kernel-Makemenu4](/img/kernel/menu4.png)

![Kernel-Makemenu5](/img/kernel/menu5.png)

![Kernel-Makemenu6](/img/kernel/menu6.png)

![Kernel-Makemenu7](/img/kernel/menu7.png)

![Kernel-Makemenu8](/img/kernel/menu8.png)

![Kernel-Makemenu9](/img/kernel/menu9.png)


#### Ongelma #1: USB laiteet/portit ei toimi.
Katselin jonkin verran netistä asioita ja kokeilin laitaa eri asetuksia make menussa päälle jotka liittyy Raspberry pi 4:sen USB toimintaan, mutta mikään ei ole auttanut so far... lisäksi koko prosessin tekeminen uudestaan ja uudestaan on hyvin pitkätekoista, jonka syystä jätän tämän asian täkseen. --> Teen uuden fresh rt installin ja ennen sitä pistän päälle ssh:n, ni teen kaikki työt terminaalissa. VNC olisi myös mahdollista, mutta terminal on itselleni tarpeeksi hyvä. Yhdistys tapahtuu itselleni komenolla:

    ssh pi@192.168.1.107

ja ssh enable terminal ohjeet löytyy täältä: https://www.raspberrypi.org/documentation/remote-access/ssh/

![Enable-SSH](/img/kernel/ssh.png)

### RT Test
Queen benchmark setup:

![Queen-Setup](/img/tests/queen.png)

Itse tulokset on test kansiossa texti tiedostossa.


### RT Ohjelmointi
Itse RT näytti simpeliltä RT Linux Foundationin ohjeiden mukaan, joka olisi c koodiin suoraan.( https://wiki.linuxfoundation.org/realtime/documentation/howto/applications/application_base). 
Toinen tapa minkä näin oli käyttää chrt metodia, joka toimii commandin yhteydessä. Tässä on linkki kyseiseen artikkeliin missä näin sen ensimäisen kerran ja itse chrt man pageen: https://lwn.net/Articles/743740/ & https://man7.org/linux/man-pages/man1/chrt.1.html

#### Ongelma #2: RT esimerkki koodi ei käänny
Tulee Print: "create pthread failed". Hakkasin päätäni tämän kanssa enemmän, kun haluaisin myöntää. Ratkaisu oli käyttää sudo komentoa koodin alussa ja tämä sama pätee chrt komennon kanssa.  Tulee error code "Bad Usage", jos sudo komentoa ei käytetä/ei ole oikeuksia.

![c-rt-test](/img/priority-test/c-rt-test.png)

#### Koodin Suunnitelu
Ensimmäinen kysymys on millä kielellä ohjelma/ohjelman osat tehdään. Vain muutama tehtävä pitää olla real time, joten ne osiot kannataisi tehdä C:llä nopeuden vuoksi. Muut osiot voisi tehdä Pythonilla... mutta chrt:n sudo osio on vähän huolestuttavaa, kun mid osiossa käytetään sudo:a kun sen voisi vaan inherittää mainista. 

--> Pyrin tekemään mahdollisimman paljon koodista C:llä ja jos tarvii käyttää Pythonia niin se onnistuu helposti system calleilla ellei tarvita return arvoja.

Elikkäs Osiot on: 
1. Dallas 18b20 luku
2. DHT22 luku + Shared Memory
4. Arvojen lähetys pilveen jollakin metodilla
3. Muita toimintoja?


#### Dallas 18b20
Hyvin samalla tyylillä mennään, kun sulatettu internet dallas python koodilla. 
1. 1-wire päälle (+restart)
2. Katsotaan saadaanko manuaalisesti arvot ulos 1-Wirestä
3. Muutetaan koodi muotoon

1-Wire setup:

![1W-setup1](/img/sensors/1wp1.png)

![1W-setup2](/img/sensors/1wp2.png)

![1W-setup3](/img/sensors/1wp2.png)


Manual dallas check:

    lsmod | grep w1
    cd /sys/bus/w1/devices/
    cd 28-xxxxxxx (Oma numero pitäisi olla)
    cat w1_slave

![dallas-Manual](/img/sensors/dallasCat.png)

Itse koodi osuus näkyy koodissa dallas.c selitykset kommenteissa...

#### DHT22 + RT
DHT22:sesta on jo anettu esimerkki koodi c kielellä --> Aikalailla install libbs ja Plug and Play, mutta suurin kysymys on miten RT asetukset laitetaan juuri tähän aplikaatioon...

Linux foundation tarjoaa hyvän listan schedulereista ja priorityistä sivulla: https://wiki.linuxfoundation.org/realtime/documentation/technical_basics/sched_policy_prio/start 

![prios](/img/priority-test/prios.png)

Eli SCHED_FIFO:n pitäisi toimia ihan hyvin, koska ei ole muita "custom" RT taskeja ja jos sen prio on korkeampi kuin muiden perus juttujen niin kaiken pitäisi olla ok. 

Testauksien jälkeen huomasin, että priority 80kin menee ihan hyvin ssh:n kautta(en tiedä onko sillä mitään oikeaa merkitystä). 
En siltikään käyttäisi sitä vain siksi kun voi... Ainut asia joka järkevästi contestii sitä on pulseaudio, joka on vain toiminnassa jos HDMI on kiinni ja sen priority on 11 (-11 htop).Tätä voidaaan muokata pulseaudio config filessä, mutta siihen ei ole tarvetta... default aplikaatio on taas 0 ja ei ole real time schedule (20 htop). Itse aplikaatio ei tarvitse ääntä taikka ruutua, jos menee ssh metodilla ja raspin ainut työ on tämä mittaus ja post. Elikkäs oman kokemuksen perusteella laittaisin 15-30 priorityn väliin. 
Tuli myös opeteltua mikä nice arvo on taskeilla, mutta ymmärykseni mukaan sillä ei ole mitään väliä RT schedulien kanssa, koska ne on "ehdotuksia" -->ei tarvitse huomioida.

nyt vaan muokata Linux Foundation koodia ja lisätä DHT22 osuus RT thread kohtaan ja siirtyä shared memoryn puolelle, että mittaukset saadaan main ohjelmaan.

DHT22 Prep:
Asennetaan tai päivitetään Wiring-Pi:

![dht-libs](/img/sensors/dht22-wiringpi.png)

DHT22 Test print muutoksilla:

![dht-code](/img/sensors/dht-confirm.png)


RT Testi Screenshotit:
Chrt Python Test:

![chrt-test](/img/priority-test/chrt-py-test.png)

Loop test + integration to main

![test-loop](/img/priority-test/looptest.png)

![main-loop](/img/sensors/dallas-dht.png)


#### loop + shared memory

Shared memoryä joutu vähän miettimään, mutta kyllä palautu mieleen ja rupes toimimaan erillisten testien kautta. Ei mitään erikoista sanottavaa tästä kuin shm metodi valittu posix ominaisuuksien ja edellisten lukujen perusteella ja koodista löytyy kommentit toimintoihin...

Man page:
https://man7.org/linux/man-pages/man7/shm_overview.7.html

Extrana lisäsin Signaali pohjaisen esteytyksen loopille, eli jos painetaan CTRL + C niin ohjelma kysyy haluatko sammuttaa ohjelman.

Toiminta molemmista:

![loop+shm](/img/sensors/loop-exit-memory.png)

#### Database 
Odotukset:
Databaseen laitto pitäisi olla yksinkertaista edellisten kokemuksien perusteella. thingsspeak ei ole itsessään tuttu, mutta jos kyseessä on iot "asia" niin luultavasti python ohjeet löytyy jos ei mitään muuta. 

Toteutus:
Tein pikaisesti mathlab tilin ja tein uuden channelin johon voidaan laitaa dataa.

![new-channel](/img/database/new-channel.png)

Pienen googlauksen jälkeen löyty python ohjeet. C ohjeita ei löytynyt. Löyty kyllä kehitys vaiheessa oleva gitlab kirjasto c kielelle, mutta koska se on kehitys vaiheessa niin en halua luoda satunnaisia muuttujia tähän työhön. Eli menen python koodilla.

Eli voi tehä c ohjelman joka käytää "system calleja" runnatakseen python koodin extra argumenteillä. Loppuunsa yksinkertainen duuni,  mutta joutui tekemään mittaus tarkistus osion mainiin ja prep ohjelman c:llä että databaseen ei lähetettä turhaa tavaraa, joka saisi chartin heilumaan liikaa.

Itse send onnistuu api:lla, joten homma ei ole luultavasti kovin vaikeaa...

![channel](/img/database/API.png)

#### Ongelma #3: Openurl toiminto ei toimi putkeen
Elikkäs tiedon lähetys tapa on openurl kautta, joka näkyy api sivulla. Alunperin tein jokaiselle fieldille oman open url komennon. Tällä tavalla ensimmäinen field menee läpi, mutta muut ei.
Hyvin arvattavissa ratkaisu oli tehdä vain 1 url joka pstaa kaikki arvot oikeisiin fieldeihin.

Pienien ongelmien jälkeen arvot menee nätisti sisään. itse chart näytää typerältä kun on niin paljon muutoksia, vaikka heittely saattaa olla esim muutama c.

![chart](/img/database/dbTest.png)


#### Extra lisäykset
Lisäsin extraksi varoitus/tila ledejä 3 kappaletta. Tilanteet olisi esim kuuma optimaali ja kylmä. Tämä oli helppo lisätä databaseInsertin toimintojen kanssa, koska operointi voi tapahtua dht:n tai dallas sensorin kautta. 

Toinen asia mitä lisäsin oli startup script, joka auto compiletaa koodin jos niin ei ole runnattavaa main fileä ja runnitaa sen tämän jälkeen. Eli ei tarvitse muistaa mitä compile optioineita pitää laittaa vaikka se on mainittu main koodissa eklla rivillä.

Eli käytetään vaan kyseistä komentoa niin kaiken pitäisi toimia:

        sudo bash startup-script.sh

![end-product](/img/sensors/endP.png)


## Loppu fiilikset
RT oli lopuunsa mukava aihe, mutta raspin usb ongelmat veivät motin näin lievästi sanottuna, ni päädyin kiertämmän sen. Tuli myös opittua enemmän kun odotin. 

Vaikeudet oli dokeumentoida, kun yrittää ilmaista odotuksia, tunteita, oleellisia asioita päiväkirjassa, kun koodin kommentointiin menee suurin osa selityksistä mitä tietty osio tai linja tekee. Pitää koittaa lukea päiväkirja uudestaan ja yrittää lisätä täydenyksiä, jos jokin ei käy järkeen tai on vaan tyhmästi ilmaistu... 

Lisäksi en ollut varma miten olisi pitänyt koodi käydä läpi, ni päätin vaan jättää sen kokonaan. Syinä olivät koodin pituus ja päiväkirjan pituus kuvien kanssa on jo valmiiksi valtava.

## Ongelma Recap
1. Usb ei toimi (ssh kierto)
2. RT thread (run as sudo)
3. openurl post (vain 1 komento)

## Commands to Operate
        sudo bash startup-script.sh

        //Manual Tarvittaessa:
        gcc ./main.c -o ./main -Wall -pthread -lwiringPi -lwiringPiDev -lrt
        sudo ./main