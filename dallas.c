int readDallas(){
    char path[50] = "/sys/bus/w1/devices/";
    char rom[20];
    char buf[100];
    DIR *dirp;
    struct dirent *direntp;
    int fd =-1;
    char *temp;
    float value;
    float errorValue = 500.0; //Dallas temp: -55-125c --> 500 can be used as a error code

    // These tow lines mount the device:
    system("sudo modprobe w1-gpio");
    system("sudo modprobe w1-therm");

    // Check if /sys/bus/w1/devices/ exists.
    if((dirp = opendir(path)) == NULL)
    {
        printf("opendir error\n");
        return errorValue; 
    }
    // Reads the directories or files in the current directory.
    while((direntp = readdir(dirp)) != NULL)
    {
        // If 28-0 is the substring of d_name,
        // then copy d_name to rom and print rom.  
        if(strstr(direntp->d_name,"28-"))
        {
            strcpy(rom,direntp->d_name);
            //printf(" rom: %s\n",rom);
        }
    }
    closedir(dirp);

    // Append the String rom and "/w1_slave" to path
    // path becomes to "/sys/bus/w1/devices/28-xxxxxxxx/w1_slave"
    strcat(path,rom);
    strcat(path,"/w1_slave");

    // Open the file in the path.
    if((fd = open(path,O_RDONLY)) < 0)
    {
        printf("open error\n");
        return errorValue;
    }
    // Read the file
    if(read(fd,buf,sizeof(buf)) < 0)
    {
        printf("read error\n");
        return errorValue;
    }
    // Returns the first index of 't'.
    temp = strchr(buf,'t');
    // Read the string following "t=".
    sscanf(temp,"t=%s",temp);
    // atof: changes string to float.
    value = atof(temp)/1000;
    //Return temperature in celsius
    return value;
}
