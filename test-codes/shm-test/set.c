//Compile: gcc ./set.c -o ./set -Wall -lrt
#include <stdio.h>
#include <sys/mman.h>
#include <fcntl.h>
#include <unistd.h>
#include <string.h>

#define STORAGE_ID "/IOT_SHM"
#define STORAGE_SIZE 32
#define DATA "%f"


int main()
{
    float value = 100;
	int res;
	int fd;
	int len;
	void *addr;
	char data[STORAGE_SIZE];

	// "Create" Shared memory file descriptor
	fd = shm_open(STORAGE_ID, O_RDWR | O_CREAT, S_IRUSR | S_IWUSR);
	if (fd == -1)
	{
		perror("open");
		return 10;
	}

	// Extend shared memory object as by default it's initialized with size 0
	res = ftruncate(fd, STORAGE_SIZE);
	if (res == -1)
	{
		perror("ftruncate");
		return 20;
	}

	// Map shared memory to process address space
	addr = mmap(NULL, STORAGE_SIZE, PROT_WRITE, MAP_SHARED, fd, 0);
	if (addr == MAP_FAILED)
	{
		perror("mmap");
		return 30;
	}

    //Test loop
    for(int i = 0; i<3;i++){
    //Format float for shm
	sprintf(data, DATA, value);

	// Place data into memory
	len = strlen(data) + 1;
	memcpy(addr, data, len);

    printf("round [%d]\n", i);

    value += 100;

	// Wait for read.c to read shm
	sleep(5);
    }

	// Clean mmap
	res = munmap(addr, STORAGE_SIZE);
	if (res == -1)
	{
		perror("munmap");
		return 40;
	}

	// Close and release shared memory
	fd = shm_unlink(STORAGE_ID);
	if (fd == -1)
	{
		perror("unlink");
		return 100;
	}

	return 0;
}
