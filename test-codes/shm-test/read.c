//Compile: gcc ./read.c -o ./read -Wall -lrt
#include <stdio.h>
#include <stdlib.h>
#include <sys/mman.h>
#include <fcntl.h>
#include <unistd.h>
#include <string.h>

#define STORAGE_ID "/IOT_SHM"
#define STORAGE_SIZE 32

int main()
{
	int fd;
	char data[STORAGE_SIZE];
	void *addr;

	// get shared memory file descriptor 
	fd = shm_open(STORAGE_ID, O_RDONLY, S_IRUSR | S_IWUSR);
	if (fd == -1)
	{
		perror("open");
		return 10;
	}

	// map shared memory to process address space
	addr = mmap(NULL, STORAGE_SIZE, PROT_READ, MAP_SHARED, fd, 0);
	if (addr == MAP_FAILED)
	{
		perror("mmap");
		return 30;
	}

	// Place data into a variable (data)
	memcpy(data, addr, STORAGE_SIZE);

	// Convert Char* to Float 
	float value = atof(data);

	value += 100;

	//Print Results
	printf("Read from shared memory: %s & %.2f\n", data, value);

	return 0;
}
