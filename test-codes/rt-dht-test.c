//Compile: gcc ./rt-dht-test.c -o rt-dht-test -Wall -lpthread -lwiringPi -lwiringPiDev
#include <limits.h>
#include <pthread.h>
#include <sched.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/mman.h>

#include <wiringPi.h>
#include <stdint.h>

#define MAX_TIMINGS	85
#define DHT_PIN		1   //GPIO18: http://wiringpi.com/pins/
 
void *thread_func(void *data)
{
        if ( wiringPiSetup() == -1 )
        exit( 1 );

        uint8_t laststate	= HIGH;
        uint8_t counter		= 0;
        uint8_t j			= 0, i;

        int dat[5] = { 0, 0, 0, 0, 0 };

        dat[0] = dat[1] = dat[2] = dat[3] = dat[4] = 0;

        /* pull pin down for 18 milliseconds */
        pinMode( DHT_PIN, OUTPUT );
        digitalWrite( DHT_PIN, LOW );
        delay( 18 );

        /* prepare to read the pin */
        pinMode( DHT_PIN, INPUT );

        /* detect change and read data */
        for ( i = 0; i < MAX_TIMINGS; i++ )
        {
            counter = 0;
            while ( digitalRead( DHT_PIN ) == laststate )
            {
                counter++;
                delayMicroseconds( 1 );
                if ( counter == 255 )
                {
                    break;
                }
            }
            laststate = digitalRead( DHT_PIN );

            if ( counter == 255 )
                break;

            /* ignore first 3 transitions */
            if ( (i >= 4) && (i % 2 == 0) )
            {
                /* shove each bit into the storage bytes */
                dat[j / 8] <<= 1;
                if ( counter > 16 )
                    dat[j / 8] |= 1;
                j++;
            }
        }

        /*
        * check we read 40 bits (8bit x 5 ) + verify checksum in the last byte
        * print it out if data is good
        */
        if ( (j >= 40) &&
            (dat[4] == ( (dat[0] + dat[1] + dat[2] + dat[3]) & 0xFF) ) )
        {
            float h = (float)((dat[0] << 8) + dat[1]) / 10;
            if ( h > 100 )
            {
                h = dat[0];	
            }
            float c = (float)(((dat[2] & 0x7F) << 8) + dat[3]) / 10;
            if ( c > 125 )
            {
                c = dat[2];	
            }
            if ( dat[2] & 0x80 )
            {
                c = -c;
            }
            //For fahrenheit
            //float f = c * 1.8f + 32;
            //printf( "Humidity = %.1f %% Temperature = %.1f *C (%.1f *F)\n", h, c, f );
            printf("%.1f,%.1f",c,h);
        }else  {
            printf("Invalid DHT22 Sensor Data");
        }
        return NULL;
}
 
int main(int argc, char* argv[])
{
        struct sched_param param;
        pthread_attr_t attr;
        pthread_t thread;
        int ret;
 
        /* Lock memory */
        if(mlockall(MCL_CURRENT|MCL_FUTURE) == -1) {
                printf("mlockall failed: %m\n");
                exit(-2);
        }
 
        /* Initialize pthread attributes (default values) */
        ret = pthread_attr_init(&attr);
        if (ret) {
                printf("init pthread attributes failed\n");
                goto out;
        }
 
        /* Set a specific stack size  */
        ret = pthread_attr_setstacksize(&attr, PTHREAD_STACK_MIN);
        if (ret) {
            printf("pthread setstacksize failed\n");
            goto out;
        }
 
        /* Set scheduler policy and priority of pthread */
        ret = pthread_attr_setschedpolicy(&attr, SCHED_FIFO);
        if (ret) {
                printf("pthread setschedpolicy failed\n");
                goto out;
        }
        param.sched_priority = 80;
        ret = pthread_attr_setschedparam(&attr, &param);
        if (ret) {
                printf("pthread setschedparam failed\n");
                goto out;
        }
        /* Use scheduling parameters of attr */
        ret = pthread_attr_setinheritsched(&attr, PTHREAD_EXPLICIT_SCHED);
        if (ret) {
                printf("pthread setinheritsched failed\n");
                goto out;
        }
 
        /* Create a pthread with specified attributes */
        ret = pthread_create(&thread, &attr, thread_func, NULL);
        if (ret) {
                printf("create pthread failed\n");
                goto out;
        }
 
        /* Join the thread and wait until it is done */
        ret = pthread_join(thread, NULL);
        if (ret)
                printf("join pthread failed: %m\n");
 
out:
        return ret;
}


