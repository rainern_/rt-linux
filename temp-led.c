//GPIOs: http://wiringpi.com/pins/
#define BLUE_LED	15      //14    //Cold Temp Led 
#define YELLOW_LED  16      //15    //Optimal Temp Led
#define RED_LED     4       //23    //Hot Temp Led

int setupTempLeds(){
    /* WiringPi Setup Check */
    if ( wiringPiSetup() == -1 ){
        printf("wiringPiSetup failed");
        return 1;
    }
    //setup led pins to output
    pinMode( BLUE_LED, OUTPUT );
    pinMode( YELLOW_LED, OUTPUT );
    pinMode( RED_LED, OUTPUT );

    //Turn off leds (using ledbar --> inverted)
    digitalWrite( BLUE_LED, HIGH );
    digitalWrite( YELLOW_LED, HIGH );
    digitalWrite( RED_LED, HIGH );

    return 0;
}

//Led switch for only Dallas
void ledSwitchDAL(float Temperature){
    //For "Hot" Temperature
    if (Temperature >= 25.0 ){
        digitalWrite( BLUE_LED, HIGH );
        digitalWrite( YELLOW_LED, HIGH );
        digitalWrite( RED_LED, LOW );
    }
    //For "Cold" Temperature
    else if (Temperature <= 0.0 ){
        digitalWrite( BLUE_LED, LOW );
        digitalWrite( YELLOW_LED, HIGH );
        digitalWrite( RED_LED, HIGH );
    }
    //For "Optimal" Temperature
    else{
        digitalWrite( BLUE_LED, HIGH );
        digitalWrite( YELLOW_LED, LOW );
        digitalWrite( RED_LED, HIGH );
    }
}

//Led switch for only DHT22
void ledSwitchDHT(float Temperature){
    //For "Hot" Temperature
    if (Temperature >= 25.0 ){
        digitalWrite( BLUE_LED, HIGH );
        digitalWrite( YELLOW_LED, HIGH );
        digitalWrite( RED_LED, LOW );
    }
    //For "Cold" Temperature
    else if (Temperature <= 0.0 ){
        digitalWrite( BLUE_LED, LOW );
        digitalWrite( YELLOW_LED, HIGH );
        digitalWrite( RED_LED, HIGH );
    }
    //For "Optimal" Temperature
    else{
        digitalWrite( BLUE_LED, HIGH );
        digitalWrite( YELLOW_LED, LOW );
        digitalWrite( RED_LED, HIGH );
    }
}

//Led switch for both dallas and dht22
void ledSwitchBOTH(float dallasTemperature, float dhtTemperature){
    //For "Hot" Temperature
    if (dallasTemperature >= 25.0 || dhtTemperature >= 25.0 ){
        digitalWrite( BLUE_LED, HIGH );
        digitalWrite( YELLOW_LED, HIGH );
        digitalWrite( RED_LED, LOW );
    }
    //For "Cold" Temperature
    else if (dallasTemperature <= 0.0 || dhtTemperature <= 0.0 ){
        digitalWrite( BLUE_LED, LOW );
        digitalWrite( YELLOW_LED, HIGH );
        digitalWrite( RED_LED, HIGH );
    }
    //For "Optimal" Temperature
    else{
        digitalWrite( BLUE_LED, HIGH );
        digitalWrite( YELLOW_LED, LOW );
        digitalWrite( RED_LED, HIGH );
    }
}