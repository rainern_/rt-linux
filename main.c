//Compile: gcc ./main.c -o ./main -Wall -pthread -lwiringPi -lwiringPiDev -lrt
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <dirent.h>
#include <string.h>
#include <time.h>
#include <limits.h>
#include <pthread.h>
#include <sched.h>
#include <sys/mman.h>
#include <wiringPi.h>
#include <stdint.h>
#include <assert.h>
#include <signal.h>

#include "dallas.c"
#include "dht.c"
#include "db-ini.c"
#include "temp-led.c"

//For Signal Handling
int loop = 0;
void     INThandler(int);

//For Shared Memory
#define STORAGE_ID "/IOT_SHM"
#define STORAGE_SIZE 32

int main(void){

    /* One time Run Section */
    // Variables
    int mode;
    int dallasFailure, dhtFailure = 0;
    char* pend;
    float dallasTemperature;
    float dhtTemperature; 
    float dhtHumidity;

    /* Shared Memory Setup: */
    // Variables:
	int res;
	int fd;
	char data[STORAGE_SIZE];
	void *addr;

    //Temperature LedIndicator Setup
    setupTempLeds();

    // "Create" Shared memory file descriptor (read only for main)
	fd = shm_open(STORAGE_ID, O_RDONLY | O_CREAT, S_IRUSR | S_IWUSR);
	if (fd == -1)
	{
		perror("open");
		return 10;
	}

    // Map shared memory to process address space
	addr = mmap(NULL, STORAGE_SIZE, PROT_READ, MAP_SHARED, fd, 0);
	if (addr == MAP_FAILED)
	{
		perror("mmap");
		return 30;
	}


    //Print exit instructions
    printf("Press Ctrl + C to exit program\n\n");
    signal(SIGINT, INThandler);

    /* Loop */
    while(!loop){
        // Read Dallas 18b20 Sensors Temperature
        dallasTemperature = readDallas();
        if(dallasTemperature != 500.0){
            printf("Dallast temperature: %.2f \n", dallasTemperature);
            //Update dallasFailure status 
            dallasFailure = 0;
        }
        else{
            printf("Dallas 18b20 reading failed\n");
            dallasFailure = 1;    
        }

        // "Activate" DHT22 process and write resoults to shared memory
        readDHT();

        // Read DHT22 sensors temperature & humidity from shared memory
        memcpy(data, addr, STORAGE_SIZE);

        // Change data to usable floats
        dhtTemperature = strtof(data, &pend);
        dhtHumidity = strtof(pend, NULL);

        // Check if dht reading happened without errors
        if(dhtTemperature != 500.0 && dhtHumidity != 500.0){
            printf("DHT22 Temperature: %.2f°C\nDHT22 Humidity: %.2f%%\n\n", dhtTemperature, dhtHumidity );
            dhtFailure = 0;
        }
	    else{
	        printf("DHT22 reading failed.\n\n");
	        dhtFailure = 1;
	    }

        //Send data to database/cloud acording read failures and change led status
        if(dallasFailure == 1 && dhtFailure == 1){
            printf("All readings have failed, not sending anything to database/cloud\n\n");
        }
        else if(dallasFailure == 1 && dhtFailure == 0){
            mode = 2;
            ledSwitchDHT(dhtTemperature);
        }
        else if(dallasFailure == 0 && dhtFailure == 1){
            mode = 1;
            ledSwitchDAL(dallasTemperature);
        }
        else{
            mode = 0;
            ledSwitchBOTH(dallasTemperature, dhtTemperature);
        }
        iniDB(mode, dallasTemperature, dhtTemperature, dhtHumidity);


        //Delay until next measurement
        sleep(300);
    }

    /* Close Functions: */
    // Clean mmap
    res = munmap(addr, STORAGE_SIZE);
    if (res == -1)
    {
        perror("munmap");
        return 40;
    }

    // Close and release shared memory
    fd = shm_unlink(STORAGE_ID);
    if (fd == -1)
    {
        perror("unlink");
        return 100;
    }

    printf("Exited out successfully\n");
    return 0;
}


//Signal Function
void  INThandler(int sig)
{
     signal(sig, SIG_IGN);
     //Comment out all sections except upper signal() and loop++ for a background task
     //Asks if you really want to quit
     printf("\nOUCH, did you hit Ctrl-C?\n"
            "Do you really want to quit? [y/n] ");
     char c = getchar();
     if (c == 'y' || c == 'Y')
        loop++;
     else
        signal(SIGINT, INThandler);
     getchar(); // Get new line character
}
